var aws = require('aws-sdk');
var AthenaExpress = require('athena-express');
var _ = require('underscore');
var config = require('./config');
var awsCredential = config();
aws.config.update(awsCredential);
var athenaExpress = new AthenaExpress({ aws });
var fpgrowth = require('node-fpgrowth');



var getData = async (dbname, tbname) => {
    var sql = `SELECT productid, orderid, orderdate FROM ${tbname}`;
    var results =  await athenaExpress.query({
        sql : sql,
        db : dbname
    });
    return results.Items;
}

var createBasketTb = (data) => {
    // var orderdate = _.pluck(data, 'orderdate');
    // var minDate = _.min(orderdate);
    // var maxDate = _.max(orderdate);
    // console.log(_.pick(data, 'orderid', 'productid'));
    var productRefs = _.chain(data).map((ele) => {return ele.productid}).uniq().map( (ele, index) => { var x = {}; x[ele.toString()] = index; return x; })
                     .reduce( (acc, currentValue) => {return _.extend(acc, currentValue)}).value();
    var transData = _.map(data, (ele) => {return _.extend(ele, {productRef : productRefs[ele.productid]})})
    var baskets = _.chain(transData).map((ele) => {return _.pick(ele, 'orderid', 'productid', 'productRef')})
                              .groupBy('orderid').mapObject((ele) => {return _.pluck(ele, 'productRef')})
                              .value();
    var transactions = _.values(baskets)
    return transactions;

}

var calFpGrowth = (transactions) => {
    try {
        
        var fpg = new fpgrowth.FPGrowth(0.005);
        // Returns itemsets 'as soon as possible' through events.
    fpg.on('data', function (itemset) {
        // Do something with the frequent itemset.
        var support = itemset.support;
        var items = itemset.items;
        console.log(`Itemset { ${items.join(',')} } is frequent and have a support of ${support}`);
    });
    // Execute FPGrowth on a given set of transactions.
fpg.exec(transactions)
.then(function (itemsets) {
  // Returns an array representing the frequent itemsets.
  console.log(`Finished executing FPGrowth. ${itemsets.length} frequent itemset(s) were found.`);
});
    } catch (error) {
       console.log(error) ;
    }
}

getData('www_google_com', 'transcational')
        .then(
            (results) => {
                baskets = createBasketTb(results);
                calFpGrowth(baskets);
                // console.log(baskets);
            },
            (error) => {
                console.log(error);
            }   
        )